/*
 * # locale library examples
 *
 * ## Web Framework for Node.js
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.1
 */

var locale = require('..');

//locale.lang = 'ru';

var i18n = new locale.i18n({ 'lang': 'ru' })
//var i18n = new locale.i18n()
  , __   = i18n.__
  , __n  = i18n.__n;

function t(cb) {
  [0, 1, 2, 5, 10, 11, 21].forEach(function(i) {
    console.log(cb(i));
  });
}

//i18n.locale = 'ru';
console.log(__());
console.log(__('Hello!'));
console.log(locale.__('Hello!'));
console.log(__('Hello?'));
console.log(__('%s(action), %s((screened)!', [ 'Bye' , 'Tom' ]));
console.log(__('%s(action), %s((screened)!', { 'action':'Bye' , 'name':'Tom' }));
console.log(__n('There is %n apple on the table.', 'There are %n apples on the table.', 1));
console.log(__n('There is %n apple on the table.', 'There are %n apples on the table.', 2));
console.log(__n('There is %n apple on the table.', 'There are %n apples on the table.', 5));
console.log(__n('There is %n monkey on the table.', 'There are %n monkeys on the table.', 1));

[ 'en', 'ru' ].forEach(function(l) {
  i18n.lang = l;
  t(function(n) {
    return __n(
    'There is %n monkey (percent sign works fine %%) on the %s in the %s.',
    'There is %n monkeys (percent sign works fine %%) on the %s in the %s.',
    n, __('table'), __('office')
  )});
});
//console.log(fill('Hi, %s(name), there is %n monkey on the %s(on) in the %s(in).', { 'on':'table', 'in':'office', 'name':'Tom' }, 1));