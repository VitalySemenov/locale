# Locale for Javascript

## It's simple

    __('Hello!');
    __n('There is %n item in your cart.', 'There are %n items in your cart.', cart.items.length);

## You can pass additional arguments to this function to fill placeholders in the i18n strings

    __('%s is the new wonderful library for %s.', 'locale', 'javascript');
    __n('There is %n %s library for %s.', 'There are %n %s libraries for %s.', n, 'good', 'javascript');

Or as array:

    __('%s to %s', [ 'left', 'right' ]);
    __n('%s: you have %n %s message.', '%s: you have %n %s messages.', count, [ 'Spam', 'unreaded' ]);

Or as object:

    __('Routes from %s(from) to %s(to):', { 'from': 'Hawaii', 'to': 'Ontario' });
    __n('Routes from %s(from) to %s(to): %n route found.', 'Routes from %s(from) to %s(to): %n routes found.', n, { 'from': 'Hawaii', 'to': 'Ontario' });

## Locale selection

Globally (for library):

    locale.lang = 'ru';

Locally (for i18n object instance):

    i18n.lang = 'de'
