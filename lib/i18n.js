/*
 * # locale library
 *
 * ## Web Framework for Node.js
 * 
 * @author Dmitry A. Chleck <dmitrychleck@gmail.com>
 * @version 0.0.1
 */

var fs    = require('fs')
  , path  = require('path');

/**
 * Translation rules for plural forms
 *
 * rules =
 * {
 *   <lang>: [ <name>, <n>, <rule> ],
 *   ...
 * }
 * , where:
 *
 *   <lang> {String} Language key.
 *   Example: 'en' for English, 'ru' for Russin etc.
 *
 *   <name> {String} Language name.
 *   Example: 'English', 'Russian' etc.
 *
 *   <n> {Numeric} Number of plural forms for this language.
 *   Example: 2 for English, 3 for Russian.
 *
 *   <rule> {String} Javascript expression which takes n and returns right plural form number.
 *   Example: '(n == 1 ? 0 : 1)' for English.
 *
 * @api private
 */
var rules = { 'en': [] };

/**
 * Translation dictionary
 *
 * tr =
 * {
 *   <lang>: {
 *     <key>: <translation>,
 *   },
 *   ...
 * }
 * , where:
 *
 *   <lang> {String} is language key.
 *   Example: 'en' for English, 'ru' for Russian etc.
 *
 *   <key>  {String} is translation key, the single or the first form of base language phrase.
 *   Example: 'Hello!' or 'There is %n object.'
 *
 *   <translation> {String}||{Array} is translation phrase(s), string for single or array of string for plural.
 *   Example: 'Привет!' or [ 'Имеется %n объект.', 'Имеется %n объекта.', 'Имеется %n объектов.']
 *
 * @api private
 */
var tr = {};

/**
 * Raise error.
 * @api private
 */
function noinit() {
  throw new Error('Forgot to init() locale library?')
}

function loadJson(file) {
  try {
    return JSON.parse(fs.readFileSync(path.join(exports.path, file + '.json')));
  } catch(e) {}
}

/**
 * Fill placeholders in given string with args
 * fill(s, args)
 * fill(s, args, n)
 * @param {String} s String with placeholders. Placeholders are %n, %s and %s(name).
 * @param {Array} args %s and %s(name) will be replaced by args values in the same order, name will be ignored.
 * @param {Object} args %s will be replaced by '', %s(name) will be replaced by args value with the same name. 
 * @param undefined n %n will be replaced by ''.
 * @param {Number} n %n will be replaced by n value.
 * @return {String} Formatted string.
 * @api private
 */
function fill(s, args, n) {
  var argc = 0                // Current %s argument position
    , chunks = s.split('%')   // String parts
    , result = [ chunks[0] ]; // Result buffer
  for(var i = 1; i < chunks.length; i++) {
    var chunk = chunks[i];
    var arg = '';
    switch(chunk[0]) {
      // %% => %
      case undefined:
        // Push '%' and next chunk
        result.push('%');
        result.push(chunks[++i]);
        continue;
      // %n => n
      case 'n':
        arg = n;
        break;
      // %s => value from args
      case 's':
        // Get name if present
        var name = '';
        // '((' after %s - ignore name
        if(chunk[1] === '(') {
          if(chunk[2] === '(') {
            // %s(( - %s with screened '('' after it but not %s(name)
            chunk = chunk.substring(1);
          } else {
            // %s(name)
            name = chunk.substr(2, chunk.indexOf(')') - 2);
            // Cut (name) at begin of chunk
            chunk = chunk.substring(name.length + 2);
          }
        }
        if(Array.isArray(args)) {
          // Next arg if array
          arg = args[argc];
        } else {
          // Named arg if object
          arg = args[name];
        }
        // Next %s
        argc++;
        break;
    }
    result.push(arg);
    result.push(chunk.substring(1));
  }
  return result.join('');
}

/**
 * See __() functions.
 * @api private
 */
function i() {
  var result
    , s
    , args = [];
  for(var i in arguments) {
    if(s == undefined) {
      s = arguments[0];
    } else {
      args.push(arguments[i]);
    }
  }
  if(typeof args[0] === 'object' || Array.isArray(args[0])) {
    args = args[0];
  }
  try {
    result = tr[this.lang][s] || s;
  } catch(e) {
    result = s;
  }
  return fill(result || '', args);
}

/**
 * See __n() functions.
 * @api private
 */
function n() {
  var n
    , result
    , f      = 0
    , forms  = []
    , args   = [];
  // Split arguments to phrase forms, number and additional args
  for(var i in arguments) {
    var a = arguments[i];
    if(typeof n === 'undefined') {
      if(typeof a === 'number') {
        n = a;
      } else {
        forms.push(a);
      }
    } else {
      args.push(a);
    }
  }
  if(typeof args[0] === 'object' || Array.isArray(args[0])) {
    args = args[0];
  }
  try {
    // Return translation
    f = rules[this.lang].f(n);
    result = tr[this.lang][forms[0]][f];
  } catch(e) {
    // Drop to base language if any error
    f = rules[exports.base].f(n);
    result = forms[f];
  }
  return fill(result || '', args, n);
}

/**
 * i18n objects constructor
 * @param {Object} options:
 *   lang: translation language (default is current global lang)
 * @return {i18n} object
 * @api public
 */
function i18n(options)
{
  var self = this;
  options = options || {};
  this.lang = options.lang || exports.lang;

  /**
   * Proxy function which uses local (object) context
   * __(<phrase>, <arg1>, <agr2>, ... ) Where phrase is {String} and arg1, arg2, ... is {String}.
   * __(<phrase>, <args>) Where phrase is {String} and args is {Array}.
   * __(<phrase>, <args>) Where phrase is {String} and args is {Object}.
   * @return {String} Translated phrase.
   * @api public
   */
  this.__ = function() { return i.apply(self, arguments); }
  /**
   * Proxy function which uses local (object) context
   * __n(<form1>, <form2>, ... , <number>, <arg1>, <agr2>, ... ) Where form1, form2, ... is {String}, number is {Numeric} and arg1, arg2, ... is {String}.
   * __n(<form1>, <form2>, ... , <number>, <args>) Where form1, form2, ... is {String}, number is {Numeric} and args is {Array}.
   * __n(<form1>, <form2>, ... , <number>, <args>) Where form1, form2, ... is {String}, number is {Numeric} and args is {Object}.
   * @return {String} Translated phrase in correct plural form.
   * @api public
   */
  this.__n = function() { return n.apply(self, arguments); }
}

/**
 * Proxy function which uses global (library) context
 * __(<phrase>, <arg1>, <agr2>, ... ) Where phrase is {String} and arg1, arg2, ... is {String}.
 * __(<phrase>, <args>) Where phrase is {String} and args is {Array}.
 * __(<phrase>, <args>) Where phrase is {String} and args is {Object}.
 * @return {String} Translated phrase.
 * @api public
 */
function __() { return i.apply(exports, arguments); }

/**
 * Proxy function which uses global (library) context
 * __n(<form1>, <form2>, ... , <number>, <arg1>, <agr2>, ... ) Where form1, form2, ... is {String}, number is {Numeric} and arg1, arg2, ... is {String}.
 * __n(<form1>, <form2>, ... , <number>, <args>) Where form1, form2, ... is {String}, number is {Numeric} and args is {Array}.
 * __n(<form1>, <form2>, ... , <number>, <args>) Where form1, form2, ... is {String}, number is {Numeric} and args is {Object}.
 * @return {String} Translated phrase in correct plural form.
 * @api public
 */
function __n() { return n.apply(exports, arguments); }

/**
 * Library initialization.
 * init()
 * @api public
 */
function init(options) {
  options = options || {};
  // Path to locale folder
  exports.path = options.path || '';
  // Try to load config
  var conf = loadJson('i18n') || {};
  // Base language of translation (translate from)
  exports.base = options.base || conf.base || 'en';
  // Global translation language (translate to)
  exports.lang = options.lang || conf.lang || exports.base;
  // Export API functions
  exports.i18n = i18n;
  exports.__ = __;
  exports.__n = __n;
  // Try to load translation rules for plural forms
  rules = loadJson('rules');
  if(!rules) throw new Error('Can\'t load languages rules.');
  // Create rules functions
  for(var lang in rules) {
    // Create default plural rule if not exists
    if(!(rules[lang] && rules[lang][2])) rules[lang] = [ lang, 2, '(n == 1 ? 0 : 1)' ];
    // Create rule function (This works only with developer's data so I think is't safe)
    rules[lang]['f'] = Function('n', 'return ' + rules[lang][2]);
    // Try to load translation for language.
    tr[lang] = loadJson(lang) || {};
  }
}

// i18n objects constructor
exports.i18n = noinit;
// References to translation functions
exports.__ = noinit;
exports.__n = noinit;
exports.init = init;

init({ 'path': '../lib' });
